function placeHolder(element){
	var standard_message = $(element).val();
	$(element).focus(
		function() {
			if ($(this).val() == standard_message)
				$(this).val("");
		}
	);
	$(element).blur(
		function() {
			if ($(this).val() == "")
				$(this).val(standard_message);
		}
	);
}

jQuery(function(){
	$('.mega-menu').dcMegaMenu({
		speed: 300
		});
		
	$("input[type='text']").each(function(index, element) {
		  placeHolder($(this));     
	});
	
	jQuery("#effect01 li").each(function(){
		jQuery(this).find("p").append("<span></span>");
	});
	
	jQuery("#effect01 li").hover(function(){
		jQuery(this).find("a").css("color","#d19e33");
		jQuery(this).find("img").stop(true, true).animate({opacity: 0.8},500);
		//jQuery(this).find("p").find("span").stop(true, true).fadeIn();
	},function() {
		jQuery(this).find("a").css("color","#262626");
		jQuery(this).find("img").stop(true, true).animate({opacity: 1},500);
		//jQuery(this).find("p").find("span").stop(true, true).fadeOut();	
	});
	
	//jQuery("#card_previews a img").hover(function(){
//		jQuery(this).stop(true, true).animate({opacity: 0.8},500);
//	},function() {
//		jQuery(this).stop(true, true).animate({opacity: 1},500);
//	});
	
	
	jQuery(".sub-container .tabs li").hover(function(){
		if(jQuery(this).hasClass("active")){return false;}
		jQuery(this).parent().find('li').removeClass('active');
		jQuery(this).addClass("active");
		var index = jQuery(this).index();
		jQuery(this).parent().next().find('.tab-item').hide(0);
		jQuery(this).parent().next().find('.tab-item').eq(index).fadeIn(300);
	});
	
	jQuery("#form-style .btn-submit").click(function(){
		var arrName = new Array();
		var arrValue = new Array();
		var flag = true;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		
		jQuery("#form-style input[type='text']").each(function(){
			var value = jQuery(this).val();
			var key = jQuery(this).attr('name');
			arrName.push(key);
			arrValue.push(value);
		});
		
		for (i=0;i<arrName.length;i++) {
			if (arrValue[i] == '') { 
				jQuery("#form-style input[name='" + arrName[i] + "']").addClass('error');
				jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
				jQuery("#form-style input[name='" + arrName[i] + "']").after("<span class='message-error'>Verplicht veld</span>");

				flag = false;
			} else if (arrName[i] == 'email') { 
				if( !emailReg.test( arrValue[i] ) ) {
					jQuery("#form-style input[name='" + arrName[i] + "']").addClass('error');
					jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
					jQuery("#form-style input[name='" + arrName[i] + "']").after("<span class='message-error'>Verplicht veld</span>");

					flag = false;
				}
			} else {
				jQuery("#form-style input[name='" + arrName[i] + "']").removeClass('error');
				jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
				flag = true;
			}
		}
		
		var textarea = jQuery("#form-style textarea").val();
		if (textarea == '') {
			jQuery("#form-style textarea").addClass('error');
			jQuery("#form-style textarea").next().remove();
			jQuery("#form-style textarea").after("<span class='message-error'>Verplicht veld</span>");
			flag = false;
		} else {
			jQuery("#form-style textarea").removeClass('error');
			jQuery("#form-style textarea").next().remove();
			flag = true;
		}
		
		if (!flag) {
			return false;
		}
	});
	
	$("#crumbs").remove().insertBefore($("#categories"));
	$("#design .description").addClass("desc-page").remove().insertAfter($("#pagecontent > h1"));
	
});