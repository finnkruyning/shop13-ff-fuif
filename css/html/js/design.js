$(function() {
    var box = $('#card_previews_horizontal');
    var inner = $('#inner');



    function pos(i)
    {
        return -i * 168 + offset;
    }

    var s = pos(index);
    var max_s = pos(0);
    var min_s = pos(count - 1);

    function scrollTo(pos, time)
    {
        if (pos > max_s) pos = max_s;
        if (pos < min_s) pos = min_s;
        s = pos;
        inner.stop().animate( { left: s }, time);
        var is_min = s <= min_s;
        var is_max = s >= max_s;
        if (is_max)
            $('#prev_page').hide();
        else
            $('#prev_page').show();
        if (is_min)
            $('#next_page').hide();
        else
            $('#next_page').show();
    }

    function scrollBy(delta, time)
    {
        scrollTo(s + delta, time);
    }

    if (double_preview == 'False') {
        $('#voor, #binnen_preview').click(function() {
            $('#binnen_select, #binnen_preview').fadeOut(200);
            $('#voor_select, #voor_preview').fadeIn(200);
        });
        $('#binnen, #voor_preview').click( function() {
            $('#voor_select, #voor_preview').fadeOut(200);
            $('#binnen_select, #binnen_preview').fadeIn(200);
        });
    }

    $('#prev_page').click(function(event) {
        scrollBy(600, 500);
    });
    $('#next_page').click(function(event) {
        scrollBy(-600, 500);
    });
    box.bind('mousewheel', function(event, delta, x, y) {
        scrollBy(delta * 100, 50);
        return false;
    });
    box.find('a').click(function() {
        var a = $(this);
        scrollTo(pos((a.index())), 100);
        $('#binnen_preview::visible, #voor_preview::visible').fadeOut(100, function() {
            location = a.attr('href');
        });
        return false;
    });
    var tx = 0;
    box.bind('touchstart', function(e){
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        var x = touch.pageX;
        tx = x;
    });
    box.bind('touchmove', function(e){
        e.preventDefault();
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        var x = touch.pageX;
        scrollBy(x - tx);
        tx = x;
    });

    $('#voor').click();
    scrollTo(pos(index));

    $(document).keydown(function (e) {
        var keyCode = e.keyCode || e.which;
        var arrow = {left: 37, up: 38, right: 39, down: 40 };

        switch (keyCode) {
            case arrow.left:
                $('.selected').prev('a').click();
            break;
            case arrow.right:
                $('.selected').next('a').click();
            break;
        }
    });
});

