 $(function() {
     if (!$("#headerlogin").length) {
         $('.topbar-menu-right').find('#navbar-login').remove();
     }
     if (!$("#headerlogout").length) {
         $('.topbar-menu-right').find('#navbar-logout').remove();
     }
     $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header

     if ($('#body_design')) {
         var backLink = $('.crumb-mobile');

         $('#crumbs').append(backLink);
     }


     //klasses toevoegen voor custom styling verschillende pagina's and activate current menuitem

     var header = $('#header');
     var loc = window.location.href; // returns the full URL


     var menu = header.find(".middlebar-menu-right");

     if (/jubileumkaarten/.test(loc)) {
         $('body').addClass('jubileumkaarten');
         menu.find("a[href*='jubileumkaarten']").addClass('active');
     } else if (/verjaardagskaarten/.test(loc)) {
         $('body').addClass('verjaardagskaarten');
         menu.find("a[href*='verjaardagskaarten']").addClass('active');
     } else if (/themafeesten/.test(loc)) {
         $('body').addClass('themafeesten');
         menu.find("a[href*='themafeesten']").addClass('active');
     } else {
         $('body').addClass('subpage');
     }

     $('.js-scroll-to').click(function(event) {
         event.preventDefault();
         var targethref = $(this).attr('href');
         var targetlink = $(targethref);
         targettop = targetlink.offset().top;
         target = targettop - 30;
         $('html, body').animate({ scrollTop: target }, 800);
     });

     $('#crumbs a[href="/"]').html('<span class="crumb-home"></i>');

     if ($('#sidebar')) {
         $("nav #filter_cards").prepend('<h3>Filters</h3><a class="filter-w" href="javascript:;">' +
             '<i class="fa fa-refresh"></i>Filters wissen</a>');
     }

     if ($(window).width() < 991) {
         $('#navbar-basket').text(function(index, text) {
             return text.replace('items', '');
         });
     }
     /** KLEUR NAAM --------------------------------------- **/


     $('.kleur').on('mouseenter', '.tag-li', function() {
         var dataTitle = $(this).attr('id').split('li-')[1]
         $(this).attr('data-title', dataTitle);
         $(this).addClass('show-title');

     });
     $('.kleur').on('mouseleave', '.tag-li', function() {
         console.log($(this).attr('id'));
         $(this).removeClass('show-title');

     });




     /* Tag scroller ------------------------------------------------------------------------------------------------- */

     if ($('.js-add-tag-scroller').length) {
         console.log(1234);
         $('.js-add-tag-scroller').append('<div class="tag-scroller"><div id="tag-scroller-prev"></div>' +
             '<div id="tag-scroller-next"></div><div class="tag-wrapper"></div></div><div class="top-cards"></div>');

         $('#card_previews a').each(function(i) {
             var $u = $(this).find('img').attr('real-src');
             if ($('#headerlogin').length > 0) {
                 if (i < 10) {
                     $(this).not('.trash_icon, .add_user_design').appendTo('.tag-wrapper');
                 } else {
                     $(this).appendTo('.top-cards');
                 }
             } else {
                 if (i < 10) {
                     $(this).not('.trash_icon, .add_user_design').find('img').attr('src', $u).parent().appendTo('.tag-wrapper');
                 } else {
                     $(this).not('.trash_icon, .add_user_design').find('img').attr('src', $u).parent().appendTo('.top-cards');
                 }
             }
         })

         $('#card_previews').remove();
     } else {
         console.log('Er is geen tag-scroller div aanwezig');
     }

     $(window).load(function() {
         if ($('.tag-wrapper').length > 0) {
             $('div.tag-wrapper').cycle({
                 timeout: 0,
                 slides: '> a',
                 prev: '#tag-scroller-prev',
                 next: '#tag-scroller-next',
                 fx: 'carousel',
                 allowWrap: true,
                 speed: 500,
                 swipe: true,
                 swipeFx: 'carousel'
             });
         };
     });



 });