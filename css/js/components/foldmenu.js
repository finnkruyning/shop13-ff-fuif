$(function () {
   $('.js-foldmenu-header .icon').on('click', function(){

       var foldmenuHeader = $(this).parent('.js-foldmenu-header')
       var foldmenu = foldmenuHeader.parent('.js-foldmenu');
       var target = foldmenu.find('.js-foldmenu-body');

       target.toggleClass('show');

       if (target.hasClass('show')){
           $(this).removeClass('icon-arrow-bold-down').addClass('icon-arrow-bold-up');
       }
       else {
           $(this).removeClass('icon-arrow-bold-up').addClass('icon-arrow-bold-down');
       }
   })

});
