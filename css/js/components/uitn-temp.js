$(document).ready(function () {
    $("body[id^='body_home']").addClass('page-home');
    var loc = window.location.href;

    if (/uitn-temp/.test(loc) && !/blog/.test(loc)) {
        $('body').addClass('uitn-page');
    }

    if (/trouwen/.test(loc) && !$('#blog_post').length) {
        $('body').addClass('trouwkaarten-page');
    }

    if ($('#uitn-temp').length) {
        if ($('#uitn-temp').hasClass('main-banner')) {
            $('body').addClass('uitn-page');
        }
    }

    var $uitnPage = $('.uitn-page'),
        $itemUspBlocks = $uitnPage.find('.usp-blocks').find('.item');

    //Remove H1 not necessary
    $uitnPage.find('#pagebody').children('h1').remove();

    //Set height usp block
    function setHeightUSBItem() {
        setTimeout(function () {
            var max = 0;

            $itemUspBlocks.each(function (index, el) {
                var temp = $(el).outerHeight();

                if (temp > max) {
                    max = temp;
                }
            });

            $itemUspBlocks.outerHeight(max);
        }, 3000);
    }

    function clickBlock() {
        $itemUspBlocks.click(function(){
            window.location.href = $(this).find('a').attr('href');
        });
    }

    clickBlock();
    setHeightUSBItem();

    if (jQuery(window).width() <= 992) {
        $('#sidebar .accordion').find('.accordion-button').removeClass('active').next().removeClass('show');
        $('#sidebar .accordion').find('.icon-chevron-down').removeClass('icon-chevron-down').addClass('icon-chevron-right');
    }

    // move card-view list to carousel

    function setCarousel() {
        if ($('.thumbnail-scroller-middle').length) {
            $('#card_previews a').each(function (i) {
                var $u = $(this).find('img').attr('real-src');
                var $imgElm = $(this).not('.trash_icon, .add_user_design').find('img');
                $imgElm.attr('src', $u).addClass("img-responsive img-center");
                $imgElm.parent().addClass("cycle-slide").appendTo('.thumbnail-scroller-middle');
            })
            $('#card_previews').remove();
        } else {
            console.log('Er is geen mainCont div aanwezig');
        }
    }

    function carouselScroller() {
        var $thumbnailScroller = $('.carousel-scroller');

        $thumbnailScroller.each(function () {
            var _self = $(this);
            _self.find('.thumbnail-scroller-middle').cycle({
                fx: 'carousel',
                slides: '> a',
                continueAuto: false,
                speed: 600,
                swipe: true,
                carouselFluid: true,
                sync: false,
            });

        });

        $thumbnailScroller.find('.thumbnail-scroller-right a').click(function (e) {
            e.preventDefault();
            var _self = $(this);
            var target = _self.closest($thumbnailScroller).find('.thumbnail-scroller-middle');
            target.cycle('next');
        });

        $thumbnailScroller.find('.thumbnail-scroller-left a').click(function (e) {
            e.preventDefault();
            var _self = $(this);
            var target = _self.closest($thumbnailScroller).find('.thumbnail-scroller-middle');
            target.cycle('prev');
        });

    }

    setCarousel();
    carouselScroller();

    if ($('#body_design').length) {
        $('.accordion.first').find('.accordion-button').addClass('active');
        $('.accordion.first').find('.accordion-body').addClass('show');
    }

    $('.topbar .topbar-menu-right .dropdown > a').click(function(e){
        if (jQuery(window).width() <= 992) {
            e.preventDefault();
            if ($(this).hasClass('opened')) {
                $(this).next().slideUp('fast');
                $(this).removeClass('opened');
                $(this).parent('.dropdown').removeClass('parent-open');
            } else {
                $(this).next().slideDown('fast');
                $(this).next().css('visibility','visible');
                $(this).addClass('opened');
                $(this).parent('.dropdown').addClass('parent-open');
            }
        }
    });

    var loc2 = window.location.href;
    if(!$('.blog_detail_content').length) {
        if(/bedankkaartjes/.test(loc2) && !$('#blog_post').length) {
            $('body').removeClass('.other-page').addClass('trouwkaarten-page');
            $('.trouwkaarten-page #header .logo img').attr('src','/img/logo-2.png');
            $('#menu-general').attr('style','display:none !important');
            $('#menu-trouwkaarten').find('.accordion-button').addClass('active').next().addClass('show');
            $('#menu-trouwkaarten').find('.icon-chevron-right').removeClass('icon-chevron-right').addClass('icon-chevron-down');
        } else if(/menukaarten/.test(loc2) && !$('#blog_post').length) {
            $('body').removeClass('.other-page').addClass('trouwkaarten-page');
            $('.trouwkaarten-page #header .logo img').attr('src','/img/logo-2.png');
            $('#menu-general').attr('style','display:none !important');
            $('#menu-trouwkaarten').find('.accordion-button').addClass('active').next().addClass('show');
            $('#menu-trouwkaarten').find('.icon-chevron-right').removeClass('icon-chevron-right').addClass('icon-chevron-down');
        } else if(/jubileumkaarten/.test(loc2)) {
            if (loc2.split("/").length > 4) {
                $('#menu-jubileumkaarten').find('.accordion-button').removeClass('active').next().removeClass('show');
                $('#menu-jubileumkaarten').find('.icon-chevron-down').removeClass('icon-chevron-down').addClass('icon-chevron-right');
            }
        } else if(/uitnodiging-verjaardag/.test(loc2)) {
            if (loc2.split("/").length > 4) {
                $('#menu-uitnodiging-verjaardag').find('.accordion-button').removeClass('active').next().removeClass('show');
                $('#menu-uitnodiging-verjaardag').find('.icon-chevron-down').removeClass('icon-chevron-down').addClass('icon-chevron-right');
            }
        } else if(/uitnodiging-feest/.test(loc2)) {
            if (loc2.split("/").length > 4) {
                $('#menu-uitnodiging-feest').find('.accordion-button').removeClass('active').next().removeClass('show');
                $('#menu-uitnodiging-feest').find('.icon-chevron-down').removeClass('icon-chevron-down').addClass('icon-chevron-right');
            }
        } else if (/trouwen/.test(loc2) && !$('#blog_post').length){
            $('.trouwkaarten-page #header .logo img').attr('src','/img/logo-2.png');
        }
    }
    //Search function
    $('a.zoek').on('click', function () {
        $(this).toggleClass('activeS');
        $(this).toggleClass('active').parent().find('form input').fadeToggle(function () {
            $('#search_content input').focus()
        });
    });

    if ($(window).width() < 768) {
        if ($('.slick-silder').length > 0) {
            $('.slick-silder').slick({
              dots: true,
              arrows: false,
              autoplay: true,
              autoplaySpeed: 3000,
              infinite: true,
              speed: 300,
              slidesToShow: 1,
              adaptiveHeight: true
            });
        }
    }

    //page jubileumkaarten

    var $mainFull = $('main.full');
    if($mainFull.length) {
        $('#categories').remove();
        $('h1').hide();
        $('#mid').addClass('full');
        $('#pagecontent').addClass('full');
        $('#crumbs').wrapInner('<div class="container"></div>');
    }
});

