$(function () {
    if($(window).width() < 767){
        $(window).scroll(function(){
            if($(document).scrollTop() > 50) {
                $('.middlebar .srchForm').hide();
                $('.navbar').css('height', '50px');
                $('#body_design .design-content').css('padding-top', '55px');
                $('.logo-mobile').css('padding-top', '55px');
                $('.navbar .collapse').css('top', '50px');
            } else {
                $('.middlebar .srchForm').show();
                $('.navbar').css('height', '95px');
                $('#body_design .design-content').css('padding-top', '100px');
                $('.logo-mobile').css('padding-top', '100px');
                $('.navbar .collapse').css('top', '95px');
            }
        });
    };
});
