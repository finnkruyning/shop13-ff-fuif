function placeHolder(element){
	var standard_message = $(element).val();
	$(element).focus(
		function() {
			if ($(this).val() == standard_message)
				$(this).val("");
		}
	);
	$(element).blur(
		function() {
			if ($(this).val() == "")
				$(this).val(standard_message);
		}
	);
}

function shuffle(o){
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};

jQuery(function(){
	var flogo = jQuery("#flogo").html();
	jQuery('body').append(flogo);
	jQuery("#flogo").remove();

	jQuery("#home").remove();
	$("input[type='text']").each(function(index, element) {
		  placeHolder($(this));
	});

	jQuery("#effect01 li").each(function(){
		jQuery(this).find("p").append("<span></span>");
	});

	jQuery("#effect01 li").hover(function(){
		jQuery(this).find("a").css("color","#d19e33");
		jQuery(this).find("img").stop(true, true).animate({opacity: 0.8},500);
	},function() {
		jQuery(this).find("a").css("color","#262626");
		jQuery(this).find("img").stop(true, true).animate({opacity: 1},500);
	});

	if(jQuery('.fotokaart').length >0) {
		jQuery('.fotokaart li').click(function(){
			var href = jQuery(this).find('a').attr('href');
			window.location.assign(href);
		});
		jQuery('.fotokaart li').hover(function(){
			jQuery(this).addClass('hover');
		},function(){
			jQuery(this).removeClass('hover');
		});
	}

	jQuery(".sub-container .tabs li").hover(function(){
		if(jQuery(this).hasClass("active")){return false;}
		jQuery(this).parent().find('li').removeClass('active');
		jQuery(this).addClass("active");
		var index = jQuery(this).index();
		jQuery(this).parent().next().find('.tab-item').hide(0);
		jQuery(this).parent().next().find('.tab-item').eq(index).fadeIn(300);
	});

	jQuery("#form-style .btn-submit").click(function(){
		var arrName = new Array();
		var arrValue = new Array();
		var flag = true;
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

		jQuery("#form-style input[type='text']").each(function(){
			var value = jQuery(this).val();
			var key = jQuery(this).attr('name');
			arrName.push(key);
			arrValue.push(value);
		});

		for (i=0;i<arrName.length;i++) {
			if (arrValue[i] == '') {
				jQuery("#form-style input[name='" + arrName[i] + "']").addClass('error');
				jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
				jQuery("#form-style input[name='" + arrName[i] + "']").after("<span class='message-error'>Verplicht veld</span>");

				flag = false;
			} else if (arrName[i] == 'email') {
				if( !emailReg.test( arrValue[i] ) ) {
					jQuery("#form-style input[name='" + arrName[i] + "']").addClass('error');
					jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
					jQuery("#form-style input[name='" + arrName[i] + "']").after("<span class='message-error'>Verplicht veld</span>");

					flag = false;
				}
			} else {
				jQuery("#form-style input[name='" + arrName[i] + "']").removeClass('error');
				jQuery("#form-style input[name='" + arrName[i] + "']").next().remove();
				flag = true;
			}
		}

		var textarea = jQuery("#form-style textarea").val();
		if (textarea == '') {
			jQuery("#form-style textarea").addClass('error');
			jQuery("#form-style textarea").next().remove();
			jQuery("#form-style textarea").after("<span class='message-error'>Verplicht veld</span>");
			flag = false;
		} else {
			jQuery("#form-style textarea").removeClass('error');
			jQuery("#form-style textarea").next().remove();
			flag = true;
		}

		if (!flag) {
			return false;
		}
	});

	$("#crumbs").remove().insertBefore($("#categories"));
	$("#design .description").addClass("desc-page").insertAfter($("#pagecontent > h1"));


	//DEMO Calculator
	$("#navCal").hover(function(event){
		$('#calculator',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		$('#calculator',this).stop(true, true).slideUp(200);
	});

	$('#navCal').find('select').click(function (e) {
		e.stopPropagation();
	});
	$('#navCal').find('select').hover(function (e) {
		e.stopPropagation();
	});

	var pathname = window.location.pathname;
	pathname_a = pathname.split("/");
	$('.mega-menu li').each(function(){
		var href = $(this).find('a').attr('href');
		var href_a = href.split("/");
		var n = pathname_a[1];

		if (pathname_a[1] == href_a[1]) {
			$(this).addClass('active');
		}
	});


	if($('#body_account').length > 0) {
		$('#body_account #pagecontent .button').eq(2).remove();
		$('#body_account #pagecontent .button').eq(2).remove();
	}

	if($('#lnav').length > 0){
		$('#lnav ul li:first-child h3').addClass('active');
		$('#lnav ul li:first-child h3.active').next('ul').show();
		$('#lnav ul li h3').each(function(){
			var href = $(this).find('a').attr('href');
			var href_a = href.split("/");
			if (href_a[1] == pathname_a[1]) {$('#lnav ul li:first-child h3').removeClass('active');$('#lnav ul li:first-child h3').next('ul').hide();$(this).addClass('active');$(this).next('ul').show();}

		});

		$('#lnav ul li ul li').each(function(){
			var href = $(this).find('a').attr('href');
			if (href == pathname) {$(this).addClass('active');}
		});
	}

	if ($('#textblock').length > 0) {
		var ht = $('#textblock').html();
		$('#categories').append(ht);
	}

	if ($('#categories nav').length > 0){
		$('#categories nav').after($('#lnav .envelop'));
	}

	$.ajax({
		type: "GET",
		url: "//fuif.nl/css/reviews-fuif.xml",
		dataType: "xml",
		success: function(xml){
			var $reviews = shuffle($("review", xml));
			var maxReviews = 3;
			$reviews.filter(":lt(" + maxReviews + ")").each(function(index, q) {
				var description = $(q).find('description').text(),
				url = $(q).find('url').text();
				$('#footer .feedback').append('<p>"' + description + '"<span><a href="' + url + '" target="_blank"> via the Feedback Company</a></span></p>');
			});
		},
		error: function() {
			$('#footer .feedback').html("An error occurred while processing XML file.");
		}
	});

	// Zoekvenster niet zichtbaar op mobiel bij scrollen
	// Zoekvenster wel weer zichtbaar als er naar de bovenkant wordt gescrolld
	if($(window).width() < 767){
        $(window).scroll(function(){
            if($(document).scrollTop() > 50) {
                $('.middlebar .srchForm').hide();
                $('.navbar').css('height', '50px');
                $('#body_design .design-content').css('padding-top', '55px');
                $('.logo-mobile').css('padding-top', '55px');
                $('.navbar .collapse').css('top', '50px');
            } else {
                $('.middlebar .srchForm').show();
                $('.navbar').css('height', '95px');
                $('#body_design .design-content').css('padding-top', '100px');
                $('.logo-mobile').css('padding-top', '100px');
                $('.navbar .collapse').css('top', '95px');
            }
        });
    };


});
